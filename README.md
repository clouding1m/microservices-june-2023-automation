# Microservices June 2023 - Unit 3 自动化发布/部署

这篇文章是四篇教程之一，旨在帮助您实践来自《****Microservices June 2023****：开始交付微服务》的概念：

- **[如何部署和配置微服务](https://www.nginx-cn.net/blog/nginx-tutorial-deploy-configure-microservices/)**
- [如何安全地管理容器中的密码](https://www.nginx-cn.net/blog/nginx-tutorial-securely-manage-secrets-containers/)
- [如何使用 Gitlab CI/CD 自动化微服务发布（本文）](https://jihulab.com/f5/microservices-june-2023-automation)
- **[如何使用 OpenTelemetry 跟踪了解您的微服务](https://www.nginx-cn.net/blog/nginx-tutorial-opentelemetry-tracing-understand-microservices/)**

# 概述

自动化部署对于大多数项目的成功至关重要。然而，仅仅部署代码是不够的。您还需要确保停机时间有限（或消除），并且在失败的情况下快速回滚。将*金丝雀部署*和*蓝绿部署*组合起来是确保新代码可行的常见方法。该策略包括两个步骤：

- **步骤1：金丝雀部署以隔离测试** - 将代码部署到环境之外的独立节点、服务器或容器中，测试以确保代码按预期工作。
- **步骤2：蓝绿部署以生产环境测试** - 假设代码在金丝雀部署中工作，将代码移植到新创建的服务器（或节点或容器）中，与当前版本的服务器并行存在于您的生产环境中。然后重定向一部分生产流量到新版本，以测试它是否在更高的负载下继续良好地工作。通常，您从将一小部分（例如10%）流量引导到新版本开始，并逐步增加它，直到新版本接收所有流量。增量的大小取决于您对新版本是否能够处理流量的信心；您甚至可以在单个步骤中完全切换到新版本。

如果您不熟悉在应用程序或网站的不同版本之间分配流量的不同用例（*流量分割*），请阅读我们博客上的文章《如何使用高级流量管理提高 Kubernetes 的弹性》了解蓝绿部署、金丝雀发布、A/B 测试、速率限制、断路器等的概念。虽然博客是针对 Kubernetes 的，但这些概念广泛适用于微服务应用程序。

# **教程概述**

在本教程中，我们展示如何使用Gitlab自动化部署的第一步。在教程的四个挑战中，您可以使用Docker Desktop或者其他K8S环境部署应用程序的新版本

- **创建和部署NGINX容器应用程序**
- **使用Gitlab CI/CD**
- **测试Gitlab CI/CD 工作流程**
- *注意：**虽然本教程使用Docker Destop K8S环境，但是这些概念和技术可以应用于任何被授权的K8S环境（例如：阿里云、腾讯云等）。

### GitLab CI/CD 简介

GitLab CI/CD 是 GitLab 提供的持续集成和持续部署的解决方案。它可以帮助团队自动化构建、测试和部署应用程序，从而提高生产力并加速交付速度。使用 GitLab CI/CD，您可以将 CI/CD 管道集成到您的开发工作流中，从而自动执行以下任务：

- 检查代码并运行测试
- 构建和打包应用程序
- 部署应用程序到各种环境（如开发环境、测试环境和生产环境）
- 监控应用程序的运行状况并自动执行故障恢复操作

GitLab CI/CD 可以与各种语言和框架一起使用，并支持多种部署方式，包括 Kubernetes、AWS、GCP、Azure 等。它还提供了一个直观且易于使用的界面，使得团队可以快速上手并开始构建 CI/CD 管道。

`极狐参考文档：[https://docs.gitlab.com/ee/administration/cicd.html](https://docs.gitlab.com/ee/administration/cicd.html)`

# Unit 3 自动化发布/部署先期准备工作

### 在你开始本实验之前，请做好一下准备工作

- K8S集群环境（GKE，EKS，AKS，AKC，笔记本Docker Desktop with Kubernetes均可）
    - 检查环境有效性：
    
    ```bash
    kubectl get all -A
    ```
    
- Gitlab([https://gitlab.com/](https://gitlab.com/))账户或者极狐（[https://jihulab.com](https://jihulab.com/))账户
- Helm环境
    - 检查helm命令安装成功
    
    ```bash
    helm version
    ```
    
- Visual Studio Code环境 或者 git 环境
    - 检查git环境
    
    ```bash
    git -v
    ```
    
- nodejs 19.x运行环境
    - 检查nodejs命令安装成功
    
    ```bash
    node -v
    ```
    

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled.png)

# 挑战1 - 安装CI/CD环境安装

## 1 - 创建分支

首先Login到你自己的Gitlab中，然后在我们这个[Repository](https://jihulab.com/f5/microservices-june-2023-automation)基础上**建立一个分支（branch），点击Fork**

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%201.png)

选择目标地。

## 2 - 克隆到本地

可以使用vscode自带的Clone Git能力或者Git命令都可以，使用vscode请参阅Demo，Git命令如下：

```bash
git clone <https://jihulab.com/f5/microservices-june-2023-automation.git>
```

<aside>
💡 注意这里将<>内的地址替换为你自己的地址

</aside>

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%202.png)

完成后你可以看到一个类似于这样的目录结构

<aside>
☝ 至此你已经完成了你的第一个Gitlab项目

</aside>

<aside>
✅ 自我检查**：**
1.  确认文件夹已经完整克隆
2. 检查git branch的输出为main

</aside>

# 挑战2 - 配置Agent Config

自动化部署是现代软件开发流程中至关重要的一环。它的目的是为了让软件交付流程更快速、更可靠。在实践中，自动化部署通常包括两种方式：推送（PUSH）和拉取（PULL）。推送通常是指将代码和配置文件推送到服务器上，而拉取则是指从代码仓库中拉取代码并自动构建和部署到服务器上。这里我们使用拉的方式（Gitops）来完成CI/CD，有兴趣的同学也可以尝试用Git Runner来完成。

## 1-编写Agent配置文件

使用Gitops需要在K8S上部署一个Agent，首先需要配置 .gitlab/agents/my-agent/config.yaml **配置文件这里已经为大家准备好，不用任何操作。**
```bash
gitops:
  manifest_projects:
  - id: "F5/Microservices-June-2023-automation"
    ref: # either `branch`, `tag`, or `commit` can be specified
      branch: main
    paths:
    - glob: '/deploy/**/*.{yaml,json}'
```

<aside>
💡 这里的id: F5/Microservices-June-2023-automation 请改为你自己的名字，my-agent可以改为你喜欢的任意名字

</aside>

这是一个 GitOps 的配置文件示例，其中有一个 `manifest_projects` 的配置项，用于指定 GitOps 管理的 Kubernetes YAML 或 JSON 配置文件所在的路径。其中，`id` 属性用于唯一标识该项目，`paths` 属性用于指定该项目包含的文件路径，使用 glob 模式进行匹配。

在此示例中，`id` 为 `F5/Microservices-June-2023-automation`，该项目包含的文件路径为 `/` 下所有的 YAML 或 JSON 文件。

glob 是一种文件名匹配模式，它允许您使用通配符字符（如 * 和 ?）来匹配文件或目录的名称。在 GitOps 的配置文件示例中，glob 模式用于匹配指定目录下的 YAML 或 JSON 文件。例如，'/**/*.yaml' 可以匹配所有子目录下的 YAML 文件。

在这个示例中，`glob` 应该被设置为 `'deploy/**/*.yaml'`，以匹配 `F5/Microservices-June-2023-automation` 项目下的 `deploy` 文件夹中的所有 YAML 文件。

要在 GitLab 中使用 GitOps 实现自动部署，您需要配置以下组件：

1. GitLab CI/CD 管道：使用 CI/CD 管道从 Git 存储库的 `deploy` 目录中提取 YAML 文件并部署它们。
2. GitLab Agent：在 Kubernetes 集群上运行的 GitLab Agent，用于执行 CI/CD 管道。

在我们的Lab中，Agent启动后，k8s就会部署这个/deploy/xc-simple-login.yaml这个配置文件

### 2 - 配置Kubernetes clusters

**点击左侧菜单栏中的Infrastructure下的Kubernetes clusters，选择<connect a cluster>**

使用以下 Helm 命令在 Kubernetes 集群中安装 GitLab Agent：

```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install my-agent gitlab/gitlab-agent \
    --namespace gitlab-agent-my-agent \
    --create-namespace \
    --set image.tag=v15.11.0-rc2 \
    --set config.token=<your key> \
    --set config.kasAddress=wss://kas.jihulab.com
```

在这个命令中，`<your key>` 应该替换为您的 GitLab key。使用极狐时创建cluster时，这个命令和key都会自动产生。

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%203.png)

大约5分钟后，my-agent就能自动部署完成。

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%204.png)

<aside>
💡 注意，由于fork操作会直接启动CI Pipeline，因此检查pipeline时会发现deploy作业不成功，这个是正常的，后续提交commit会再次执行pipeline，或者等my-agent部署完直接retry。

</aside>

<aside>
✅ 自我检查**：**
1. 使用`helm list -A`检查agent的部署情况
2. 使用`kubectl get all -A` 检查 deploy, service, pod的启动情况

</aside>

# 挑战3 - 配置CI

**这里已经为大家准备好，不用任何操作。**

```
stages:          # List of stages for jobs, and their order of execution
  - package
  - deploy

# Use the official docker image.

docker-build:       # This job runs in the package stage
  stage: package
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - echo "Dockering application..."
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile

deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  environment: dev
  image:
    name: bitnami/kubectl:latest
    entrypoint: [""]
  script:
    - echo "Deploying application..."
    - kubectl config get-contexts
    - kubectl config use-context "$CI_PROJECT_NAMESPACE"/"$CI_PROJECT_NAME":my-agent
    - echo "kubectl set image deployment/xc-simple-login xc-simple-login=$CI_REGISTRY_IMAGE:latest"
    - kubectl set image deployment/xc-simple-login xc-simple-login=$CI_REGISTRY_IMAGE:latest
    - kubectl rollout restart deployment/xc-simple-login
    - kubectl get pod
    - echo "Application successfully deployed."
  only:
    - main
```

该文件定义了名为 `package` 和`deploy` 的作业，分别使用 `docker` 镜像和`bitnami/kubectl`来部署。第一个package依赖于根目录下的Dockerfile，依据这个Dockerfile

```docker
FROM nginx

COPY dist/ /usr/share/nginx/html/
```

这里表示用nginx做为模板，将/dist封装入docker中，从而产生docker image文件，`$CI_REGISTRY_IMAGE${tag}` 这个是gitlab的内置变量，是镜像仓库的位置。

### 效果检查-CI/CD效果

此时你在k8s上已经能够看到部署完成的pod和svc了。

如果在实验过程中用的是Docker Desktop K8S或者你是本地部署，那么使用就可以将port映射到localhost上，那么可以直接用浏览器访问http://localhost:30010

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%205.png)

然后我们修改`/src/views/Home.vue`这个文件，将`Welcome to F5 Simple Login App`修改为`你喜欢的名字`，例如**“这是我的第一个网页”**，然后重新编译，执行

```bash
npm run build
```

并提交（commit）后（可以使用vscode，或者用git命令）

将更改添加到暂存区：

```
git add .
```

提交更改：

```
git commit -m "update"

```

然后你在jihulab上就能看到新的pipeline了

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%206.png)

在k8s上能够自动部署最新的镜像文件

此时，当你再次打开浏览器，就能看到最新的名字

<aside>
✅ 自我检查：
1. 检查上面pipeline截图的2个stage都是绿色的✅状态
2. 检查本地能够打开网页

</aside>

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%207.png)

![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%208.png)

# 思考题

如何果大家想像demo中自己编译VUE，需要执行：

```bash
npm install -D @vue/cli-service
npm run build
```

那如何将npm也封装在CI/CD中？这样只要改变了代码就能直接部署了。

<aside>
🤔 可以将修改的CI文件放在jihulab中，如果你提交URL，我们会检查你的配置文件，从而获得额外加分。

</aside>

# 最终检查

## 检查1-代码库正确Fork

LAB的仓库代码已 Fork 至您的仓库中

并且在CI/CD中流水线均正常结束（✅状态）
![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%209.png)

## 检查2-deploy-job的输出结果
![Untitled](Microservices%20June%202023%20-%20Unit%203%20%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8F%91%E5%B8%83%20%E9%83%A8%E7%BD%B2%2092535ea22020415883c76e49e7a7c79f/Untitled%2010.png)

确保红框标注的内容存在于deploy-job的输出结果中。

# **实验验收标准：**
请同学们在提交本单元的单元小测时，附上你的gitlab/jihulab地址，以便我们验收你的配置文件。

## 提交`极狐`或者`GitLab`的URL，得分点情况如下

1. URL提交正确（能够打开的代码仓库）
2. my-agent文件内容改写正确（改为自己的项目名字，参见[挑战2](#1-编写agent配置文件)）
3. CI/CD下的流水线中最后的流水线为已通过（Passed）状态（[参见最终检查](#检查1-代码库正确fork)）
4. deploy-job的输出结果中包含一个正在创建的container和一个running的container（[参见最终检查](#检查2-deploy-job的输出结果)）
5. 附加题：流水线中包含第三个编译作业，或者Dockerfile包含编译作业

# **下一步**

恭喜！您已经学会了如何使用Gitops来执行微服务应用程序的自动发布与部署。

如果您还对Github的CI/CD感兴趣，可以查看：

[延伸阅读：如何使用 GitHub Action 实现微服务灰度发布自动化](https://www.nginx-cn.net/blog/nginx-tutorial-github-actions-automate-microservices-canary-deployments/)
